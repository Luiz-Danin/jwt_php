<?php
$key = '123456789';

$header = [
            'typ'=>'JWT',
            'alg'=>'HS256'
          ];

$payload = [
            'exp'=> (new DateTime("now"))->getTimestamp(),
            'uid'=>1,
            'email'=>'emailteste@emailteste.com'
           ];

//JSON
$header = json_encode($header);
$payload = json_encode($payload);

//BASE 64
$header = base64_encode($header);
$payload = base64_encode($payload);

$sing = hash_hmac('sha256', $header.'.'.$payload, $key, true);//assinatuta Binária = TRUE
$sing = base64_encode($sing);

//Token
print $header.'.'.$payload.'.'.$sing;