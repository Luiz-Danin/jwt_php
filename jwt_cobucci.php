<?php
require __DIR__.'/vendor/autoload.php';

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Hmac\Sha256;

$signer = new Sha256();

$time = time();

$token = (new Builder())->issuedBy('http://example.com')
                        ->permittedFor('http://example.com')
                        ->identifiedBy('4f1g23a12aa', true)
                        ->issuedAt($time)
                        ->callOnlyBeUsedAfter($time + 60)
                        ->expiresAt($time + 3600)
                        ->withClain('uid', 1)
                        ->withClain('emailteste.com', 'emailteste.com')
                        ->getToken($sing, new Key('testing') );

print $token;